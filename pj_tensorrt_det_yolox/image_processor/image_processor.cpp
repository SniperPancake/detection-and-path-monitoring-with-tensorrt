/* Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
/*** Include ***/
/* for general */
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <memory>

// for predictions
#include <Python.h>
#include "pyhelper.hpp"

/* for OpenCV */
#include <opencv2/opencv.hpp>

/* for My modules */
#include "common_helper.h"
#include "common_helper_cv.h"
#include "bounding_box.h"
#include "detection_engine.h"
#include "tracker.h"
#include "image_processor.h"

/*** Macro ***/
#define TAG "ImageProcessor"
#define PRINT(...)   COMMON_HELPER_PRINT(TAG, __VA_ARGS__)
#define PRINT_E(...) COMMON_HELPER_PRINT_E(TAG, __VA_ARGS__)

/*** Global variable ***/
std::unique_ptr<DetectionEngine> s_engine;
Tracker s_tracker;
//std::vector < cv::Point >* predictedPoints = new std::vector < cv::Point >;
std::vector < cv::Point > predictedPoints;
int32_t frameCounter = 0;

/*** Function ***/
static void DrawFps(cv::Mat& mat, double time_inference, cv::Point pos, double font_scale, int32_t thickness, cv::Scalar color_front, cv::Scalar color_back, bool is_text_on_rect = true)
{
    char text[64];
    static auto time_previous = std::chrono::steady_clock::now();
    auto time_now = std::chrono::steady_clock::now();
    double fps = 1e9 / (time_now - time_previous).count();
    time_previous = time_now;
    snprintf(text, sizeof(text), "Frame: %d, FPS: %.1f, Inference: %.1f [ms]", frameCounter, fps, time_inference);
    CommonHelper::DrawText(mat, text, cv::Point(0, 0), 0.5, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(180, 180, 180), true);
}

static cv::Scalar GetColorForId(int32_t id)
{
    static constexpr int32_t kMaxNum = 100;
    static std::vector<cv::Scalar> color_list;
    if (color_list.empty()) {
        std::srand(123);
        for (int32_t i = 0; i < kMaxNum; i++) {
            color_list.push_back(CommonHelper::CreateCvColor(std::rand() % 255, std::rand() % 255, std::rand() % 255));
        }
    }
    return color_list[id % kMaxNum];
}

int32_t ImageProcessor::Initialize(const ImageProcessor::InputParam& input_param)
{
    if (s_engine) {
        PRINT_E("Already initialized\n");
        return -1;
    }

    s_engine.reset(new DetectionEngine());
    if (s_engine->Initialize(input_param.work_dir, input_param.num_threads) != DetectionEngine::kRetOk) {
        s_engine->Finalize();
        s_engine.reset();
        return -1;
    }
    return 0;
}

int32_t ImageProcessor::Finalize(void)
{
    if (!s_engine) {
        PRINT_E("Not initialized\n");
        return -1;
    }

    std::ofstream file;
    file.open("coordinates.csv");
    file << "frame,X,Y" << std::endl;
    auto& track_list = s_tracker.GetTrackList();
    for (auto& track : track_list) {
        auto& track_history = track.GetDataHistory();
        for (size_t i = 0; i < track_history.size(); i++) {
            file << i + 1 << "," << track_history[i].bbox.x + track_history[i].bbox.w / 2 << "," << track_history[i].bbox.y + track_history[i].bbox.h << std::endl;
        }
    }
    file.close();
    //delete predictedPoints;
    if (s_engine->Finalize() != DetectionEngine::kRetOk) {
        return -1;
    }

    return 0;
}


int32_t ImageProcessor::Command(int32_t cmd)
{
    if (!s_engine) {
        PRINT_E("Not initialized\n");
        return -1;
    }

    switch (cmd) {
    case 0:
    default:
        PRINT_E("command(%d) is not supported\n", cmd);
        return -1;
    }
}


PyObject* ImageProcessor::vectorToTuple_Float(const std::vector< int32_t >& data) {
    PyObject* tuple = PyTuple_New(data.size());
    for (unsigned int i = 0; i < data.size(); i++) {
        PyObject* num = PyFloat_FromDouble((double)data[i]);
        PyTuple_SET_ITEM(tuple, i, num);
    }

    return tuple;
}

PyObject* ImageProcessor::vectorVectorToTuple_Float(const std::vector< std::vector< int32_t > >& data) {
    PyObject* tuple = PyTuple_New(data.size());
    for (unsigned int i = 0; i < data.size(); i++) {
        PyObject* subTuple = NULL;
        subTuple = vectorToTuple_Float(data[i]);
        PyTuple_SET_ITEM(tuple, i, subTuple);
    }

    return tuple;
}

cv::Point ImageProcessor::Predict(const std::vector < std::vector < int32_t > >& pixels, int32_t frames, int32_t forecast)
{
    

    PyObject* dataset = NULL;
    PyObject* framesNumber = NULL;
    PyObject* forecastFrames = NULL;
    dataset = vectorVectorToTuple_Float(pixels);
    framesNumber = PyLong_FromLong(long(frames));
    forecastFrames = PyLong_FromLong(long(forecast));

    CPyObject pName = PyUnicode_FromString("VAR_forecast");
    CPyObject pModule = PyImport_Import(pName);
    cv::Point PyValues;
    std::vector<int> PyVector;
    if (pModule)
    {
        CPyObject pFunc = PyObject_GetAttrString(pModule, (char*)"forecast");
        if (pFunc && PyCallable_Check(pFunc))
        {
            CPyObject pArgs = PyTuple_Pack(3, dataset, framesNumber, forecastFrames);
            CPyObject pValue = PyObject_CallObject(pFunc, pArgs);
            PyObject* valueX = PyList_GetItem(pValue, 0);
            PyObject* valueY = PyList_GetItem(pValue, 1);
            for (Py_ssize_t i = 0; i < PyList_Size(pValue); i++) {
                PyObject* value = PyList_GetItem(pValue, i);
                PyVector.push_back(PyFloat_AsDouble(value));
            }
            PyValues = cv::Point(PyVector[0], PyVector[1]);
        }

    }

    return PyValues;
}

/*int32_t ImageProcessor::Predict(std::vector < int32_t > pixels)
{
    int32_t p_position = pixels[0], p_velocity = 0, p_s_velocity = 0, p_acceleration = 0, p_s_acceleration;
    int32_t c_position = pixels[1], c_velocity = 0, c_s_velocity = 0, c_acceleration = 0, c_s_acceleration;
    constexpr int32_t kNumPredictedFrames = 3, kNumCalcFrames = 3;
    std::array<int32_t, kNumPredictedFrames> f_position;


    for (size_t i = 0; i < kNumCalcFrames - 1; i++) {


        // +Velocity
        if (pixels[i + 1] >= pixels[i]) {
            c_velocity = pixels[i + 1] - pixels[i];
            c_s_velocity = 1;
        }
        // -Velocity
        if (pixels[i + 1] < pixels[i]) {
            c_velocity = pixels[i] - pixels[i + 1];
            c_s_velocity = 0;
        }
        // Accelerating
        if (c_velocity >= p_velocity) {
            c_acceleration = c_velocity - p_velocity;
            c_s_acceleration = c_s_velocity;
        }
        // Decelerating
        if (c_velocity < p_velocity) {
            c_acceleration = p_velocity - c_velocity;
            c_s_acceleration = c_s_velocity ^ 1;
        }
        // Inflection Point
        if ((c_s_velocity ^ p_s_velocity) == 1) {
            c_acceleration = c_position + p_position;
            c_s_acceleration = c_s_velocity;
        }

        p_velocity = c_velocity;
        p_acceleration = c_acceleration;
        p_s_velocity = c_s_velocity;
    }
    //Next, the path is predicted_x by incrementally adding the acceleration to the velocity
    //then this sum to the position

    f_position[0] = pixels[kNumCalcFrames - 2]; //previous
    f_position[1] = pixels[kNumCalcFrames - 1]; //current

    for (size_t i = 1; i < kNumPredictedFrames; i++) {
        if (c_s_velocity != 0) {
            if (c_s_acceleration != 0) {
                f_position[i] = f_position[i - 1] + c_velocity + c_acceleration;
            }
            if (c_s_acceleration == 0) {
                f_position[i] = f_position[i - 1] + c_velocity - c_acceleration;
            }
        }
        if (c_s_velocity == 0) {
            if (c_s_acceleration != 0) {
                f_position[i] = f_position[i - 1] - c_velocity + c_acceleration;
            }
            if (c_s_acceleration == 0) {
                f_position[i] = f_position[i - 1] - c_velocity - c_acceleration;
            }
        }
    }

    return f_position[kNumPredictedFrames - 1];
}*/

int32_t ImageProcessor::Process(cv::Mat& mat, ImageProcessor::Result& result)
{
    if (!s_engine) {
        PRINT_E("Not initialized\n");
        return -1;
    }

    DetectionEngine::Result det_result;
    if (s_engine->Process(mat, det_result) != DetectionEngine::kRetOk) {
        return -1;
    }

    /* Display target area  */
    cv::rectangle(mat, cv::Rect(det_result.crop.x, det_result.crop.y, det_result.crop.w, det_result.crop.h), CommonHelper::CreateCvColor(0, 0, 0), 2);

    /* Display detection result (black rectangle) */
    int32_t num_det = 0;
    for (const auto& bbox : det_result.bbox_list) {
        //if(bbox.class_id == 1)
        cv::rectangle(mat, cv::Rect(bbox.x, bbox.y, bbox.w, bbox.h), CommonHelper::CreateCvColor(0, 0, 0), 1);
        num_det++;
    }

    /* Display tracking result  */

    s_tracker.Update(det_result.bbox_list);
    int32_t num_track = 0;
    auto& track_list = s_tracker.GetTrackList();
    for (auto& track : track_list) {
        if (track.GetDetectedCount() < 2) continue;
        const auto& bbox = track.GetLatestData().bbox;
        /* Use white rectangle for the object which was not detected but just predicted */
        cv::Scalar color = bbox.score == 0 ? CommonHelper::CreateCvColor(255, 255, 255) : GetColorForId(track.GetId());
        //if (bbox.class_id == 1)
        cv::rectangle(mat, cv::Rect(bbox.x, bbox.y, bbox.w, bbox.h), color, 2);
        CommonHelper::DrawText(mat, std::to_string(track.GetId()) + ": " + bbox.label, cv::Point(bbox.x, bbox.y), 0.35, 1, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(220, 220, 220));
        

        auto& track_history = track.GetDataHistory();
        for (size_t i = 1; i < track_history.size(); i++) {
            cv::Point p0(track_history[i].bbox.x + track_history[i].bbox.w / 2, track_history[i].bbox.y + track_history[i].bbox.h);
            cv::Point p1(track_history[i - 1].bbox.x + track_history[i - 1].bbox.w / 2, track_history[i - 1].bbox.y + track_history[i - 1].bbox.h);
            //if (i >= track_history.size() - 150 || track_history.size() <= 150) { //draw line for last 150 frames
                //cv::line(mat, p0, p1, CommonHelper::CreateCvColor(255, 0, 0));
            //}
            //cv::line(mat, p0, p1, CommonHelper::CreateCvColor(255, 0, 0));      //draw whole history
        }

        /*Average trajectory for whole history*/
        //cv::Point p0(track_history[0].bbox.x + track_history[0].bbox.w / 2, track_history[0].bbox.y + track_history[0].bbox.h);
        //cv::Point p1(track_history[track_history.size()-1].bbox.x + track_history[track_history.size() - 1].bbox.w / 2, track_history[track_history.size() - 1].bbox.y + track_history[track_history.size() - 1].bbox.h);
        //cv::line(mat, p0, p1, CommonHelper::CreateCvColor(0, 255, 0));

        /*Average trajectory from last x frames*/
        int32_t frames = 15;
        int32_t framesToForecast = 1;
        int32_t framesToCollect = 15;
        //float distanceFactor = 1.0;
        cv::Point predictedPos;
        cv::Point old_p1;
        cv::Point predictPositionOld(0,0);
        cv::Point predictPosition(1,1);
        std::vector <std::vector < int32_t > > coordinates;

        for (size_t i = frames; i < track_history.size(); i+=frames) {    //  i < track_history.size() for whole history 
            cv::Point p0(track_history[i].bbox.x + track_history[i].bbox.w / 2, track_history[i].bbox.y + track_history[i].bbox.h);
            cv::Point p1(track_history[i - frames].bbox.x + track_history[i - frames].bbox.w / 2, track_history[i - frames].bbox.y + track_history[i - frames].bbox.h);
            //x_coordinates.push_back(p0.x);
            //y_coordinates.push_back(p0.y);
            coordinates.push_back({ p0.x, p0.y });
            //if (i >= track_history.size() - 150 || track_history.size() <= 150) {
                cv::line(mat, p0, p1, CommonHelper::CreateCvColor(0, 255, 0), 1);
                cv::circle(mat, p0, 1, CommonHelper::CreateCvColor(255, 0, 0), 3);
                cv::circle(mat, p1, 1, CommonHelper::CreateCvColor(255, 0, 0), 3);
            //}
            if (coordinates.size() > framesToCollect) {
                //if (predictPositionOld == predictPosition) cv::line(mat, p0, predictPositionOld, CommonHelper::CreateCvColor(0, 0, 255), 1);
                //predictPosition = Predict(coordinates);
                //cv::circle(mat, predictPosition, 3, CommonHelper::CreateCvColor(0, 0, 255), 2);
                coordinates.erase(coordinates.begin());
                //coordinates.erase(coordinates.begin());
                //predictPositionOld = predictPosition;
            }        
        }
        if (coordinates.size() > 3) {
            /*if (predictedPoints->size() > 0) {
                for (int32_t i = 0; i < predictedPoints->size(); i++) {
                    cv::line(mat, cv::Point(coordinates[coordinates.size()-i-1][0], coordinates[coordinates.size() - i - 1][1]), predictedPoints->at(i), CommonHelper::CreateCvColor(0, 0, 255), 1);
                }
            }*/
            if (frameCounter % frames == 0) {
                predictPosition = Predict(coordinates, coordinates.size(), framesToForecast);
                predictedPoints.push_back(predictPosition);
                predictPositionOld = predictPosition;
            }
            if (predictedPoints.size() > framesToForecast) {
                for (int32_t p = 0; p < predictedPoints.size() - framesToForecast; p++) {
                    cv::circle(mat, predictedPoints[p], 1, CommonHelper::CreateCvColor(0, 0, 255), 3);
                }
            }          
        }
        
        coordinates.clear();
        //std::cout << "clear" << std::endl;
        //std::cout << "===========================================" << std::endl;
        //y_coordinates.clear();

        num_track++;
    }
    CommonHelper::DrawText(mat, "DET: " + std::to_string(num_det) + ", TRACK: " + std::to_string(num_track), cv::Point(0, 20), 0.7, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(220, 220, 220));

    DrawFps(mat, det_result.time_inference, cv::Point(0, 0), 0.5, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(180, 180, 180), true);

    /* Return the results */
    int32_t bbox_num = 0;
    for (auto& track : track_list) {
        const auto& bbox = track.GetLatestData().bbox;
        result.object_list[bbox_num].class_id = bbox.class_id;
        snprintf(result.object_list[bbox_num].label, sizeof(result.object_list[bbox_num].label), "%s", bbox.label.c_str());
        result.object_list[bbox_num].score = bbox.score;
        result.object_list[bbox_num].x = bbox.x;
        result.object_list[bbox_num].y = bbox.y;
        result.object_list[bbox_num].width = bbox.w;
        result.object_list[bbox_num].height = bbox.h;
        /*Drawing help lines*/
        //cv::circle(mat, cv::Point(bbox.x + bbox.w/2, bbox.y + bbox.h), 10, CommonHelper::CreateCvColor(255, 0, 0));
        //cv::circle(mat, cv::Point(mat.cols/2, mat.rows), 100, CommonHelper::CreateCvColor(255, 0, 0), 5);
        //cv::line(mat, cv::Point(bbox.x + bbox.w / 2, bbox.y + bbox.h), cv::Point(mat.cols / 2, mat.rows), CommonHelper::CreateCvColor(0, 0, 255), 3);
        bbox_num++;
        if (bbox_num >= NUM_MAX_RESULT) break;
    }
    result.object_num = bbox_num;

    result.time_pre_process = det_result.time_pre_process;
    result.time_inference = det_result.time_inference;
    result.time_post_process = det_result.time_post_process;

    frameCounter++;

    return 0;
}

